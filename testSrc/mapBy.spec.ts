import { expect } from "chai";
import { values, mapBy } from "../src";

describe("mapBy", function (): void {
  it("should return mapped entities", async function (): Promise<void> {
    // arrange
    const src = [
      { f1: 1, f2: 2, f3: 1 },
      { f1: 2, f2: 1, f3: 2 },
      { f1: 1, f2: 2, f3: 3 },
      { f1: 2, f2: 1, f3: 4 },
      { f1: 1, f2: 2, f3: 5 },
    ];

    // act
    const map = mapBy(src, item => item.f1, item => item.f2);

    // assert
    const val = values(map);
    expect(val.length).equals(2);
    expect(val[0]).deep.equals(src[4]);
    expect(val[1]).deep.equals(src[3]);
  });

  it("should work with value key as well", async function (): Promise<void> {
    // arrange
    const src = [
      { f1: 2, f2: 1, f3: 4 },
      { f1: 1, f2: 2, f3: 5 },
    ];

    // act
    const map = mapBy(src, item => item.f1);

    // assert
    const val = values(map);
    expect(val.length).equals(2);
    expect(val[0]).deep.equals(src[0]);
    expect(val[1]).deep.equals(src[1]);
  });

  it("should get by key value", async function (): Promise<void> {
    // arrange
    const src = [
      { f1: 2, f2: 1, f3: 4 },
      { f1: 1, f2: 2, f3: 5 },
    ];

    // act
    const map = mapBy(src, item => item.f1);
    const result = map.get(1);

    // assert
    expect(result).deep.equals(src[1]);
  });

  it("should return mapped entities by singular selector", async function (): Promise<void> {
    // arrange
    const src = [
      { f1: 1, f2: 2, f3: 1 },
      { f1: 2, f2: 1, f3: 2 },
      { f1: 1, f2: 2, f3: 3 },
      { f1: 2, f2: 1, f3: 4 },
      { f1: 1, f2: 2, f3: 5 },
    ];
    const keySelector = item => ({ f1: item.f1, f2: item.f2 });

    // act
    const map = mapBy(src, keySelector);

    // assert
    const val = values(map);
    expect(val.length).equals(2);
    expect(val[0]).deep.equals(src[4]);
    expect(val[1]).deep.equals(src[3]);
    expect(map.has(keySelector(src[0]))).equals(true);
  });
});

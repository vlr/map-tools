import { KeyMap } from "../src";
import { expect } from "chai";

describe("keyMap", function (): void {
  it("should add with key", async function (): Promise<void> {
    // arrange
    const value = {};
    const key = { a: "1", b: 2 };
    const target = new KeyMap<any, any>(null);

    // act
    target.addWithKey(value, key);
    const result = target.get(key);

    // assert
    expect(result).equals(value);
  });

  it("should delete item", async function (): Promise<void> {
    // arrange
    const value = {}, value2 = {};
    const key = { a: "1", b: 2 };
    const target = new KeyMap<any, any>(null);
    target.addWithKey(value, key);

    // act
    target.delete(key);
    const has = target.has(key);
    target.addWithKey(value2, key);
    const result = target.get(key);

    // assert
    expect(has).equals(false);
    expect(result).equals(value2);
  });

  it("should work with 3 field key", async function (): Promise<void> {
    // arrange

    const key1 = { 0: "1", 1: "2", 2: "2", 3: "3" };
    const key2 = { 0: "1", 1: "4", 2: "2", 3: "3" };
    const target = new KeyMap<any, any>(null);

    // act
    target.addWithKey(1, key1);
    target.addWithKey(2, key2);

    // assert
    expect(target.get(key1)).equals(1);
    expect(target.get(key2)).equals(2);
  });
});

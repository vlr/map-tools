import { nullObject } from "@vlr/object-tools";

export function create<T>(): T {
  return nullObject<T>();
}

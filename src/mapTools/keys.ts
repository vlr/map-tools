import { KeyGroup } from "./complexKeyMaps/keyGroup";

export function keys<TKey, TVal>(map: Map<TKey, TVal> | KeyGroup<TKey, TVal>): TKey[] {
  return Array.from(map.keys());
}

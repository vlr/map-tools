export type Val = number | string | boolean;
export type Sel<T, T1 extends Val> = (i: T) => T1;
export type KeySelector<T, TKey> = (i: T) => TKey;

export type Key2<T1, T2> = { 0: T1, 1: T2 };
export type Key3<T1, T2, T3> = { 0: T1, 1: T2, 2: T3 };
export type Key4<T1, T2, T3, T4> = { 0: T1, 1: T2, 2: T3, 3: T4 };
export type Key5<T1, T2, T3, T4, T5> = { 0: T1, 1: T2, 2: T3, 3: T4, 4: T5 };

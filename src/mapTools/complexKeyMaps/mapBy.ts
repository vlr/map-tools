import { KeyMap } from "./keyMap";
import { KeySelector, Val, Sel, Key2, Key3, Key4, Key5 } from "./types";
import { createKey } from "./createKey";
import { IMap } from "../../objectMap/interfaces";

export function mapBy<T, TKey>(arr: T[], selector: KeySelector<T, TKey>): KeyMap<TKey, T>;
export function mapBy<T, T1 extends Val>(arr: T[], selector: Sel<T, T1>): KeyMap<T1, T>;
export function mapBy<T, T1 extends Val, T2 extends Val>
  (arr: T[], s1: Sel<T, T1>, s2: Sel<T, T2>): KeyMap<Key2<T1, T2>, T>;
export function mapBy<T, T1 extends Val, T2 extends Val, T3 extends Val>
  (arr: T[], s1: Sel<T, T1>, s2: Sel<T, T2>, s3: Sel<T, T3>): KeyMap<Key3<T1, T2, T3>, T>;
export function mapBy<T, T1 extends Val, T2 extends Val, T3 extends Val, T4 extends Val>
  (arr: T[], s1: Sel<T, T1>, s2: Sel<T, T2>, s3: Sel<T, T3>, s4: Sel<T, T4>): KeyMap<Key4<T1, T2, T3, T4>, T>;
export function mapBy<T, T1 extends Val, T2 extends Val, T3 extends Val, T4 extends Val, T5 extends Val>
  (arr: T[], s1: Sel<T, T1>, s2: Sel<T, T2>, s3: Sel<T, T3>, s4: Sel<T, T4>, s5: Sel<T, T5>): KeyMap<Key5<T1, T2, T3, T4, T5>, T>;
export function mapBy<T>(arr: T[], ...selectors: Sel<T, any>[]): KeyMap<IMap<any>, T> {
  const selector = selectors.length === 1
    ? selectors[0]
    : item => createKey(item, selectors);
  return new KeyMap(selector, arr);
}

import { Sel } from "./types";
import { IMap } from "../../objectMap/interfaces";
import { nullObject } from "@vlr/object-tools";

export function createKey<T>(item: T, by: Sel<T, any>[]): IMap<any> {
  return by.reduce((result, sel, index) => { result[index] = sel(item); return result; }, nullObject());
}

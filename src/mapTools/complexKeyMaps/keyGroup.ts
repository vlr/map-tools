import { getKeyStore } from "./keyStore/getKeyStore";
import { KeyStore } from "./keyStore";

export class KeyGroup<TKey, TVal> {
  private keyStore: KeyStore<TKey, TVal>;
  private valuesMap: Map<TKey, TVal[]> = new Map<TKey, TVal[]>();

  constructor(public keySelector: (val: TVal) => TKey, items: TVal[] = []) {
    this.keyStore = getKeyStore<TKey, TVal>(keySelector);
    items.forEach(item => this.add(item));
  }

  public values(): TVal[][] {
    return Array.from(this.valuesMap.values());
  }

  public keys(): TKey[] {
    return Array.from(this.valuesMap.keys());
  }

  public add = (item: TVal) => this.addItem(item, this.keyStore.getOrSetKeyFor(item));

  public addWithKey = (item: TVal, key: TKey) => this.addItem(item, this.keyStore.getOrSetKey(key));

  public delete(key: TKey): void {
    key = this.keyStore.deleteKey(key);
    this.valuesMap.delete(key);
  }

  public deleteBy(item: TVal): void {
    this.delete(this.keySelector(item));
  }

  public get(key: TKey): TVal[] {
    const existing = this.keyStore.getKey(key);
    return existing != null ? this.valuesMap.get(existing) : null;
  }

  public getBy(item: TVal): TVal[] {
    return this.get(this.keySelector(item));
  }

  public has(key: TKey): boolean {
    const existing = this.keyStore.getKey(key);
    return existing != null && this.valuesMap.has(existing);
  }

  public hasBy(item: TVal): boolean {
    return this.has(this.keySelector(item));
  }

  private addItem(item: TVal, key: TKey): void {
    let items = this.get(key);
    if (!items) {
      this.valuesMap.set(key, [item]);
    } else {
      items.push(item);
    }
  }
}

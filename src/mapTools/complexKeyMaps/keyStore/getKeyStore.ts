import { KeyStore } from "./keyStore";

export function getKeyStore<TKey, TVal>(keySelector: (val: TVal) => TKey): KeyStore<TKey, TVal> {
  return new KeyStore<TKey, TVal>(keySelector);
}

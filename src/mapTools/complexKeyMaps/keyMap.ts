import { KeyStore, getKeyStore } from "./keyStore";

export class KeyMap<TKey, TVal> {
  private keyStore: KeyStore<TKey, TVal>;
  private valuesMap: Map<TKey, TVal> = new Map<TKey, TVal>();

  constructor(public keySelector: (val: TVal) => TKey, items: TVal[] = []) {
    this.keyStore = getKeyStore(keySelector);
    items.forEach(item => this.add(item));
  }

  public values(): TVal[] {
    return Array.from(this.valuesMap.values());
  }

  public keys(): TKey[] {
    return Array.from(this.valuesMap.keys());
  }

  public add(item: TVal): void {
    const key = this.keyStore.getOrSetKeyFor(item);
    this.valuesMap.set(key, item);
  }

  public addWithKey(item: TVal, key: TKey): void {
    key = this.keyStore.getOrSetKey(key);
    this.valuesMap.set(key, item);
  }

  public delete(key: TKey): void {
    key = this.keyStore.deleteKey(key);
    this.valuesMap.delete(key);
  }

  public deleteBy(item: TVal): void {
    this.delete(this.keySelector(item));
  }

  public get(key: TKey): TVal {
    const existing = this.keyStore.getKey(key);
    return existing != null ? this.valuesMap.get(existing) : null;
  }

  public getBy(item: TVal): TVal {
    return this.get(this.keySelector(item));
  }

  public has(key: TKey): boolean {
    const existing = this.keyStore.getKey(key);
    return existing != null && this.valuesMap.has(existing);
  }

  public hasBy(item: TVal): boolean {
    return this.has(this.keySelector(item));
  }
}

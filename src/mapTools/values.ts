import { KeyGroup } from "./complexKeyMaps/keyGroup";
import { KeyMap } from "./complexKeyMaps/keyMap";

export function values<TKey, TVal>(map: Map<TKey, TVal>): TVal[];
export function values<TKey, TVal>(map: KeyGroup<TKey, TVal>): TVal[][];
export function values<TKey, TVal>(map: KeyMap<TKey, TVal>): TVal[];
export function values<TKey, TVal>(map: Map<TKey, TVal> | KeyGroup<TKey, TVal> | KeyMap<TKey, TVal>): TVal[] {
  return Array.from(<any>map.values());
}

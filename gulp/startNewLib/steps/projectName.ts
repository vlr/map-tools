
export function argument(): string {
  return process.argv[4];
}

export function projectName(): string {
  return argument().replace(/^.*\//, "");
}

export function projectPath(): string {
  const match = argument().match(/(?:(.+)\/)?(.+)/);
  return `${match[1] || "vlr"}/${match[2]}`;
}
